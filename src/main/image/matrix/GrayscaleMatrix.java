package main.image.matrix;

import main.helpers.ArrayHelper;

public class GrayscaleMatrix {

	public static GrayscaleMatrix createFromRegion(
			final GrayscaleMatrix source, final int width, final int height,
			final int startPixelX, final int startPixelY) {
		if (width <= 0 || height <= 0 || width > source.width
				|| height > source.height) {
			throw new IllegalArgumentException("Illegal sub region size!");
		}

		GrayscaleMatrix output = new GrayscaleMatrix(width, height);

		for (int i = 0; i < output.data.length; i++) {
			int xOffset = i % width;
			int yOffset = i / width;

			int index = ArrayHelper.convert2DTo1D(startPixelX + xOffset,
					startPixelY + yOffset, source.width);
			output.data[i] = source.data[index];
		}

		return output;
	}

	private final float[] data;

	private final int width;

	private final int height;

	public GrayscaleMatrix(final int width, final int height) {
		this.data = new float[width * height];
		this.width = width;
		this.height = height;
	}

	public GrayscaleMatrix(final int[] pixels, final int width, final int height) {
		this(width, height);

		if (width * height != pixels.length) {
			throw new IllegalArgumentException(
					"Pixels array does not match specified width and height!");
		}

		for (int i = 0; i < this.data.length; i++) {
			this.data[i] = convertRGBToGrayscale(pixels[i]);
		}
	}

	private float convertRGBToGrayscale(final int rgbColor) {
		// extract components
		int red = (rgbColor >> 16) & 0xFF;
		int green = (rgbColor >> 8) & 0xFF;
		int blue = rgbColor & 0xFF;

		// convert to grayscale
		return 0.3f * red + 0.59f * green + 0.11f * blue;
	}

	public float[] getData() {
		return this.data;
	}
	public int getHeight() {
		return this.height;
	}
	public int getWidth() {
		return this.width;
	}

}
