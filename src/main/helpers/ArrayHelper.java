package main.helpers;

public class ArrayHelper {

	public static int convert1DtoX(final int index, final int arrayWidth) {
		return index % arrayWidth;
	}

	public static int convert1DtoY(final int index, final int arrayWidth) {
		return index / arrayWidth;
	}

	public static int convert2DTo1D(final int x, final int y,
			final int arrayWidth) {
		return y * arrayWidth + x;
	}
}
