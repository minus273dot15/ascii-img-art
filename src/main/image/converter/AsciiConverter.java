package main.image.converter;

import main.helpers.ArrayHelper;
import main.image.AsciiImgCache;
import main.image.fillament.FillProposal;
import main.image.matrix.GrayscaleMatrix;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Map;

public class AsciiConverter {

    public abstract class AsciiConverter<Output> {
        protected AsciiImgCache characterCache;
        protected FillProposal fillProposal;
        protected Output output;

        public AsciiConverter(final AsciiImgCache characterCache,
                              final FillProposal fillProposal) {
            this.characterCache = characterCache;
            this.fillProposal = fillProposal;
        }

        protected abstract void addCharacterToOutput(
                final Map.Entry<Character, GrayscaleMatrix> characterEntry,
                final int[] sourceImagePixels, final int tileX, final int tileY,
                final int imageWidth);

        public Output convertImage(final BufferedImage source) {
            Dimension tileSize = this.characterCache.getCharacterImageSize();

            int outputImageWidth = (source.getWidth() / tileSize.width)
                    * tileSize.width;
            int outputImageHeight = (source.getHeight() / tileSize.height)
                    * tileSize.height;

            int[] imagePixels = source.getRGB(0, 0, outputImageWidth,
                    outputImageHeight, null, 0, outputImageWidth);

            GrayscaleMatrix sourceMatrix = new GrayscaleMatrix(imagePixels,
                    outputImageWidth, outputImageHeight);

            TiledGrayscaleMatrix tiledMatrix = new TiledGrayscaleMatrix(
                    sourceMatrix, tileSize.width, tileSize.height);

            this.output = initializeOutput(outputImageWidth, outputImageHeight);

            for (int i = 0; i < tiledMatrix.getTileCount(); i++) {

                GrayscaleMatrix tile = tiledMatrix.getTile(i);

                float minError = Float.MAX_VALUE;
                Map.Entry<Character, GrayscaleMatrix> bestFit = null;

                for (Map.Entry<Character, GrayscaleMatrix> charImage : characterCache) {
                    GrayscaleMatrix charPixels = charImage.getValue();

                    float error = this.fillProposal.calculateError(
                            charPixels, tile);

                    if (error < minError) {
                        minError = error;
                        bestFit = charImage;
                    }
                }

                int tileX = ArrayHelper.convert1DtoX(i, tiledMatrix.getTilesX());
                int tileY = ArrayHelper.convert1DtoY(i, tiledMatrix.getTilesX());
                addCharacterToOutput(bestFit, imagePixels, tileX, tileY,
                        outputImageWidth);
            }

            finalizeOutput(imagePixels, outputImageWidth, outputImageHeight);

            return this.output;

        }

        protected abstract void finalizeOutput(final int[] sourceImagePixels,
                                               final int imageWidth, final int imageHeight);

        public FillProposal getFillProposal() {
            return this.fillProposal;
        }

        protected abstract Output initializeOutput(final int imageWidth,
                                                   final int imageHeight);

        public void setCharacterCache(final AsciiImgCache characterCache) {
            this.characterCache = characterCache;
        }

        public void setFillProposal(
                final FillProposal fillProposal) {
            this.fillProposal = fillProposal;
        }
    }
}
